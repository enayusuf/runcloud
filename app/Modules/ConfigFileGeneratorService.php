<?php

namespace App\Modules;

use Illuminate\Support\Facades\File;

class ConfigFileGeneratorService
{
    protected $username;
    protected $max_memory;
    function __construct($username, $max_memory)
    {
        $this->username = $username;
        $this->max_memory = $max_memory;
    }

    public function generate()
    {
        $masterConfig = File::get(storage_path('app/config-files/mysite.conf'));

        $newConfig = str_replace(['USERNAME', 'MEMORY'], [$this->username, $this->max_memory], $masterConfig);

        File::put(storage_path('app/config-files/' . $this->username . '.conf'), $newConfig);
    }
}
