<?php

namespace App\Console\Commands;

use App\Modules\ConfigFileGeneratorService;
use Illuminate\Console\Command;

class CreatePhpFpmConfigFileCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'config-file:generate-fpm-conf {--username=} {--max_memory=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //get username from options
        $userName = $this->option('username');

        //get maxmemory
        $maxMemory = $this->option('max_memory');

        if ($userName === null) {
            abort('500','Username required');
        }

        if ($userName === null) {
            abort('500','Username required');
        }


        $configGenerator = new ConfigFileGeneratorService($userName, $maxMemory);
        $configGenerator->generate();

        return 0;
    }
}
